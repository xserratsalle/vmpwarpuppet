class ejercicio {
  file { '/www/index.php':
    ensure => file,
    replace => true,
    content => 'Hello world';
  }
  file { "/www/project1":
    ensure => "directory",
  }
  file { '/www/project1/index.php':
    ensure => file,
    replace => true,
    source  => "puppet:///modules/ejercicio/index.php",
    require => File ["/www/project1"]
  }
}

